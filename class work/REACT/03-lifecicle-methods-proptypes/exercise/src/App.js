import { Component } from 'react';
import Header from "./components/Header";
import Footer from "./components/Footer";
import PostsContainer from "./components/PostsContainer";


class App extends Component {
   

    // async componentDidMount() {
    //     this.setState({ isLoading: true })

    //     try {
    //         const data = await fetch('https://ajax.test-danit.com/api/json/posts').then(res => res.json());
    //         this.setState({ posts: data || [], isLoading: false })

    //     } catch(e) {
    //         this.setState({ isLoading: false })
    //     }

    // }

    state = {
        posts: [],
        isLoading: false,
        userId: null,

    }

    async componentDidMount() {
        this.setState({ isLoading: true })
       const posts = await fetch('https://ajax.test-danit.com/api/json/posts')
       .then( res => res.json()) 
        this.setState({posts, isLoading: false})
    }


changeUserId = (userId) => {
    console.log('helloooo');
this.setState({ userId })
}

    render(){
        
        const { posts, isLoading, userId } = this.state;

    return (
        <div className="App">
            <Header title="PropTypes"  user={{ name: 'Sam', age: 26,  avatar: 'https://i.pravatar.cc/40'}} />
            <PostsContainer posts={posts} isLoading={isLoading} changeUserId={this.changeUserId} userId={userId} />
            <Footer title="PropTypes" year={new Date().getFullYear()} onOrderFunc={() => console.log('Order call')} />
        </div>
    );
  }
}

export default App;

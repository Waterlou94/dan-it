import React, { PureComponent } from 'react';
import Post from "../Post";
import styles from './PostsContainer.module.scss';
import Preloader from "../Preloader";
import  PropTypes  from 'prop-types';
import Select from '../Select/Select';


class PostsContainer extends PureComponent {
    render(){
        const { posts, isLoading, userId, changeUserId } = this.props;

        return (
              <section className={styles.root}>
                     <Select changeUserId={changeUserId} userId={userId} />
                  <h1>POSTS</h1>
                      <div className={styles.postsContainer}>
                          {isLoading
                              ? <Preloader color="secondary" size={60} />
                              : <>{posts.map(({id, ...args}) => <Post key={id} {...args} />)}</>}
                      </div>
              </section>
        );
    }


};


PostsContainer.propTypes = {
    posts: PropTypes.array,
    isLoading: PropTypes.bool,
}

PostsContainer.defaultProps = {
    posts: [],
    isLoading: false,
}


export default PostsContainer;

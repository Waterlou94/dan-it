import React, {PureComponent} from "react";

class Mail extends PureComponent{
    render() {
        const {from, topic, body,isRead, readMail, id} = this.props

        console.log('isRead', isRead);
        return(
            <div className="email__container" onClick={() => readMail(id)}>
                <span className="email__from">{ from }</span>
                <h3 className="email__topic">{ topic }</h3>
                <p className="email__body">{ body }</p>
                {isRead ? <p>Read</p> : <p>no read</p>}
            </div>
        )

    }
}

export default Mail
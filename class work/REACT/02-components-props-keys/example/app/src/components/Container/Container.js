import React, { PureComponent } from "react";
import Button from "../Button";

class Container extends PureComponent {

    render() {
        const { text, handleClick } = this.props;


        return (
            <div>
                <p>{text}</p>
                <Button handleClick={() => handleClick('Title from COntainer')}> Change title </Button>
            </div>
        )
    }
}

export default Container;
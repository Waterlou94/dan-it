import React, { PureComponent } from "react";
import styles from './Button.module.scss';

class Button extends PureComponent {

    render() {
        console.log(this)
        const { children, handleClick } = this.props;


        return (
            <button onClick={handleClick} className={styles.btn}>{children}</button>
        )
    }
}

export default Button;
import React from "react";
import SomethingWentWrong from "../SomethingWentWrong";

class ErrorBoundary extends React.Component {
    state = { hasError: false };

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    componentDidCatch(error, info) {
        // You can also log the error to an error reporting service
        console.warn(error)
    }

    render() {
        if (this.state.hasError) {
            // You can render any custom fallback UI
            return <SomethingWentWrong />;
        }

        return this.props.children;
    }
}

export default ErrorBoundary;
import {PureComponent} from "react";
import getNameWithTime from "../../utils/getNameWithTime";

class Image extends PureComponent {

    constructor() {
        getNameWithTime('Image', 'CONSTRUCTOR');
        super();
    }

    logScroll = () => {
        console.log(Math.random(), window.scrollY);
    }

    componentDidMount() {
        getNameWithTime('Image','DID MOUNT');
        window.addEventListener('scroll', this.logScroll);
    }

    componentDidUpdate() {
        getNameWithTime('Image','DID UPDATE');

    }

    componentWillUnmount() {
        getNameWithTime('Image','WILL UNMOUNT');
        window.removeEventListener('scroll', this.logScroll)
    }


    render(){
        getNameWithTime('Image','RENDER');
        const { src } = this.props;

        console.log(src.a.a)

        return (
            <img style={{ backgroundColor: 'lightgray' }} src={src} alt="Some alt" width={400} height={200} />
        );
    }
}

export default Image;

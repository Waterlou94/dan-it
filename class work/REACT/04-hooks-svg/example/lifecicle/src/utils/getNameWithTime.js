const getNameWithTime = (name, method) => console.log(`${name}: ${method} - ${new Date().getMinutes()}:${new Date().getSeconds()}:${new Date().getMilliseconds()}`);

export default getNameWithTime;

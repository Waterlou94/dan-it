import React from 'react';
import styles from './Cart.scss';
import CartItem from "../CartItem";

const Cart = (props) => {
    const { items } = props;

    return (
        <section className={styles.root}>
            <h2>CART</h2>
            <div className={styles.items}>
                {items && items.map(item => <CartItem {...item} />)}
            </div>
            <p>Price: 0$</p>
        </section>
    )
}

export default Cart;
let items = [
    {
        id: '1',
        name: 'Orange',
        price: 300,
        image: 'https://cdn.shopify.com/s/files/1/0409/2562/6532/products/online-gulayan-makati-orange-1pc-17411997663396_grande.jpg?v=1594059033',
        isFavourite: false,
    },
    {
        id: '2',
        name: 'Lime',
        price: 500,
        image: 'https://static.libertyprim.com/files/familles/lime-large.jpg?1569491474',
        isFavourite: false,
    },
    {
        id: '3',
        name: 'Lemon',
        price: 400,
        image: 'https://www.collinsdictionary.com/images/full/lemon_234304936.jpg',
        isFavourite: false,
    }
];

module.exports = items;

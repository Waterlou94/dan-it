const express = require('express');
const colors = require('colors');
const cors = require('cors')
const bodyParser = require("body-parser");
const routes = require('./routes');
const PORT = 3001;

const app = express();
app.use(bodyParser.json());
app.use(cors());

routes(app);

app.listen(PORT, () => {
    console.log('================================================'.white)
    console.log(`Host is listening on PORT: ${PORT}`.white);
    console.log(`http://localhost:${PORT}/`.cyan);
    console.log('================================================'.white)
});

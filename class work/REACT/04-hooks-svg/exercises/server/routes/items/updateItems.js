const items = require('../../bdMock/items');

const updateItems = (app) => {
    app.put('/items', (req, res) => {

        if(req.headers && req.headers['content-type']) {
            if (!(/application\/json/gi.test(req.headers['content-type']))){
                return res.status(418).send({
                    status: `error`,
                    error: `Wrong Content-type header: expected 'application/json' instead of '${req.headers['content-type']}'`
                })
            }
        }

        const { items: itemsReq } = req.body;

        if (!itemsReq) {
            return res.status(400).send({
                status: `error`,
                error: `items is required`
            })
        }

        items.length = 0;
        items.push(...itemsReq);

        return res.status(200).send(JSON.stringify({
            status: 'success',
            data: [...items],
        }))
    })
}

module.exports = updateItems;

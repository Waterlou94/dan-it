/**
 * Task - 01
 *
 * Create any text element and container with 5 buttons inside.
 *
 * The task is to show inner text of the button that was just pressed.
 * Only one event listener can be used.
 * */
const container = document.getElementById('root')

const text = document.createElement('p')
container.prepend(text)

const arrayBtn = [
	{
		textContent: 'btn - 1',
		id: 1,
	},
	{
		textContent: 'btn - 2',
		id: 2,
	},
	{
		textContent: 'btn - 3',
		id: 3,
	},
	{
		textContent: 'btn - 4',
		id: 4,
	},
	{
		textContent: 'btn - 5',
		id: 5,
	},
	{
		name: 'email',
		id: 5,
        type: 'email',
        required: true
	},
]

arrayBtn.forEach(item => {
	const btn = document.createElement('button')
	for (let key in item) {
		btn[key] = item.key
	}
	container.append(btn)
})

// for (let i = 1; i < 6; i++) {
//     const btn = document.createElement('button')
//     btn.textContent = `btn - ${i}`
//     btn.id = i
//     container.append(btn)
// }

container.addEventListener('click', function (event) {
	if (event.target.localName === 'button') {
		text.textContent = event.target.textContent
	}
})

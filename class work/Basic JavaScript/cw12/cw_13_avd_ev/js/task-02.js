/**
 * Task - 02
 *
 * Create your custom modal window.
 *
 * CSS and markup are placed in html and css files of this classwork.
 *
 * The modal window should appears after click on button "Show modal".
 *
 * Modal window should close after clicking "cross" btn on the top right corner of it, or after clicking anywhere on the screen except the modal itself.
 * */
// const modal = document.querySelector('.modal-wrapper');

// const closeModal = document.querySelector('.modal-close');
// const btn = document.createElement('button');
// document.body.prepend(btn);
// btn.textContent = "Открыть модалку";

// btn.addEventListener('click', () => {
// modal.style.display = 'block';
// modal.style.display = 'flex';


// });
// modal.addEventListener('click', (event) =>{
//     console.log("eventTarget==== >", event.target);
//     console.log("event.currentTarget==== >", event.currentTarget);
//     if(event.target === event.currentTarget || event.target.classList.contains('modal-close')) { 
//         modal.style.display = 'none';
//     }
// });



const showModalBtn = document.createElement('button');
document.body.prepend(showModalBtn);
showModalBtn.textContent = 'Модалка';
function createModal(text) {
	const modal = document.createElement('div')
	modal.className = 'modal-wrapper'
    modal.style.display = 'flex'

	modal.insertAdjacentHTML(
		'afterbegin',
		`<div class="modal">
            <button class="modal-close">x</button>
            <p class="modal-text">${text}</p>
        </div>`,
	)

	modal.addEventListener('click', function (ev) {
		if (ev.target === ev.currentTarget || ev.target.classList.contains('modal-close')) {
			modal.remove()
		}
	})

	return modal
}

showModalBtn.addEventListener('click', () => {
	const modalInBody = createModal('Lorem ipsum dolor sit amet, consectetur adipisicing elit')
	document.body.prepend(modalInBody)
})
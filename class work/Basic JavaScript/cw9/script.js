/* TASK - 1
* Create a square.
* Ask the user about the size and background color of the square.
* Create an element in JS
* Ask the size.
* Ask the background color.
* Add the styles to the element in JS
* Place the element BEFORE the first script tag on your page
*/



// const div = document.createElement('div');
// const color = prompt('Введите цвет', 'цвет');
// const size = parseInt(prompt('Введите размер квадрата', '10'));
// div.style.backgroundColor = color;
// div.style.width = size + 'px';
// div.style.height = size + 'px';

// const script = document.querySelector('script');
// // script.insertAdjacentHTML('beforebegin', 'div');
// script.before(div);

/* TASK - 2
* Create two squares, using the same way that described in the previous task.
* But these squares are going to have different background colors.
* Create two elements in JS
* Ask the user about the size for both of these squares
* Ask the user about the fist background-color
* Ask the user about the second background-color
* Add the styles to both squares
* Place both squares BEFORE the first element with the script tag on the page
*/

// const squareOne = document.createElement('div');
// const squareTwo = document.createElement('div');

// let colorSquareOne = prompt('Введите цвет', 'blue');
// let colorSquareTwo = prompt('Введите цвет', 'yellow');
// let size = parseInt(prompt('Введите размер', '60'));

// squareOne.style.backgroundColor = colorSquareOne;
// squareTwo.style.backgroundColor = colorSquareTwo;

// squareOne.style.width = size + 'px';
// squareOne.style.height = size + 'px';
// squareTwo.style.width = size + 'px';
// squareTwo.style.height = size + 'px';
// // squareOne.style.marginBottom =  '10px';


// const script = document.querySelector('script');
// script.before(squareOne);
// script.before(squareTwo);


// Оптимизировать задачу 2 под функцию

    const squareOne = document.createElement('div');
const squareTwo = document.createElement('div');

let colorSquareOne = prompt('Введите цвет', 'blue');
let colorSquareTwo = prompt('Введите цвет', 'yellow');
let size = parseInt(prompt('Введите размер', '60'));

squareOne.style.backgroundColor = colorSquareOne;
squareTwo.style.backgroundColor = colorSquareTwo;


squareOne.style.width = size + 'px';
squareOne.style.height = size + 'px';
squareTwo.style.width = size + 'px';
squareTwo.style.height = size + 'px';
// squareOne.style.marginBottom =  '10px';


const script = document.querySelector('script');
script.before(squareOne);
script.before(squareTwo);


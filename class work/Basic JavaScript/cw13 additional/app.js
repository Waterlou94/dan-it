// повторяем объекты
 
//  const obj = {
//      key: 'value',
//      getValue(params) {
//          const that = this; // в области видимости вне функции this вынесли в переменную
// // если у нас  в методе объекта есть функция то он не увидит this как наш объект obj
// function name() {
//     console.log(this.key); // undefined
//     console.log(that.key); // выведет value
// }

//           // obj.key и this.key =  тоже самое  
//           name();   
//  },

// };
// obj.getValue(10); // в метод объекта getValue при вызове его передаем аргумет 10
// //  console.log(Object.keys(obj)); // метод объекта Object.keys выведет все ключи объекта в виде массива.

// повторяем методы объекта
// const obj1 = {
//     name: 'John',
// };

// for(let key in obj1) {
//     console.log(obj1[key]);
// }


// метод Object.entries 
// let obj2 = {name: 'John',
// }

// console.log(Object.entries(obj2));


/* МАССИВЫ*/


// const sub = document.getElementById('sub');


// sub.addEventListener('click', (event) => {
//    let data = {};
   
   
//    input.forEach((inputElement) => {
//       data[inputElement.name] = inputElement.value;
//    });
//    console.log(data);
// });



// Добавляем в объект данные с инпута введенные пользователем. Событие может быть keydown или blur
// const input = document.querySelectorAll('.field');
// let data = {};
// input.forEach((item) => {
// item.addEventListener('blur' /*можно keydown */, (event) => {
//    data[item.name] = event.target.value;
//    console.log(data);
// });
// });


   // ЗАДАЧА 
   // There is some `mailStorage` - a little simulation of real storage with email letters.
const mailStorage = [
   {
     subject: "Hello world",
     from: "gogidoe@somemail.nothing",
     to: "lolabola@ui.ux",
     text: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"
   },
   {
     subject: "How could you?!",
     from: "ladyboss@somemail.nothing",
     to: "ingeneer@nomail.here",
     text: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"
   },
   {
     subject: "Acces denied",
     from: "info@cornhub.com",
     to: "gogidoe@somemail.nothing",
     text: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"
   }
 ];
 

const parent = document.querySelector('.emails');





function showEmails (data) {
   
   data.forEach((item) => {
   const wrapper = document.createElement('div');
   wrapper.className = 'email-item';
   
   wrapper.insertAdjacentHTML('afterbegin', `
   <h4 class="email-subject">${item.subject}</h4>
   
   <div class="email-subtext-wrapper">
   <a href="mailto:${item.from}" class="email-subtext email-from">${item.from}</a>
   <a href="mailto: ${item.to}" class="email-subtext email-to">saribeg@gmail.com</a>
   </div>
   
   <p class="email-text" hidden>${item.text}</p>
   `);
   
   
   parent.append(wrapper);
   
   const formSubmit = document.getElementById('submit');
   
   wrapper.addEventListener('click', function (event){
      const text = event.currentTarget.querySelector('.email-text');

text.hidden = !text.hidden;
      
   });
      
      


   });
}
showEmails(mailStorage);
 
 
 /*** THE TASK IS ***
  * 1) Show up all the emails on the screen, using only JS creating elements.
  *    Every letter should have hidden text. Show text, only after the user clicks on the emails item.
  *    It is necessary to use only one event listener for container with letters.
  *
  * 2) Implement toggle text effect. It means that only one text on only letter can be showed at the same time.
  *    If user made a click on the letter, which doesn't has showed text - you need to close current opened text, and only then open the text for the letter that was just clicked.
  *
  * 3) Create "New Mail" button. After pressing this button user need to see a modal window with the form for create mew email letter.
  *    Fields for this form are: Email Title, To (email of the recipient), letter text (up to 500 symbols), "Send" button. By the way, you need automatically fill the 'from' property for each letter, and it will be "gogi@gogimail.go".
  *    If user want to close the modal window, he can do this, by clicking on the cross sign at the top right corner of the modal window.
  *    Modal window size is 500px both width and height. It shows up at the bottom right corner of the page, user can get access to any other part of functionality while modal window is showing.
  *
  * 4) Every letter needs to have a "Delete button, which will delete this particular letter both from the page and from the mailStorage.
  * */
 
  const showModalBtn = document.getElementById('create');


function handleModal(text) {
	const modal = document.createElement('div')
	modal.className = 'modal-wrapper'

	modal.insertAdjacentHTML(
		'afterbegin',
		`<div class="modal">
            <button class="modal-close">x</button>
            <form>
            <input name="subject" class="field" type="text">
            <input name="from" class="field" type="text">
            <input name="to" class="field" type="text">
            <textarea class="field" name="" id="" cols="30" rows="10"></textarea>
            <input class="sumbit" type="sumbit">
         </form>
      
        </div>`,
	)

	modal.addEventListener('click', function (ev) {
		if (ev.target === ev.currentTarget || ev.target.classList.contains('modal-close')) {
			modal.remove()
		}
	})

    document.body.prepend(modal)
	// return modal
}

showModalBtn.addEventListener('click', () => {
     handleModal('Lorem ipsum dolor sit amet, consectetur adipisicing elit')
    // const result = 	 handleModal('Lorem ipsum dolor sit amet, consectetur adipisicing elit')
    
    // document.body.prepend(result)
})





//    const wrapper = document.createElement('div');
// wrapper.className = 'email-item';
   
// wrapper.insertAdjacentHTML('afterbegin', `
// <h4 class="email-subject">${item.subject}</h4>

// <div class="email-subtext-wrapper">
// <a href="mailto:${item.from}" class="email-subtext email-from">${item.from}</a>
// <a href="mailto: ${item.to}" class="email-subtext email-to">saribeg@gmail.com</a>
// </div>

// <p class="email-text" hidden>${item.text}</p>
// `);


// parent.append(wrapper);


// wrapper.addEventListener('click', function (event){
//    const text = event.currentTarget.querySelector('.email-text');

// text.hidden = !text.hidden;
   
// });
   
const form = document.getElementById('createEvent')
const title = document.getElementById('eventTitle')
const date = document.getElementById('eventDate')
const desc = document.getElementById('eventDesc')
const btn = document.querySelector('.event-form button')

function makeEventObject(title, date, desc) {
	console.log(title.value)
	console.log(date.value)
	console.log(desc.value)
	/** ЗАДАНИЕ
	 * реализовать код функции  makeEventObject
	 * эта функция принимает 3 аргумента - обьекты инпутов формы
	 *  чтобы получить значние какого либо инпута достаточно обратиться к его свойству value
	 * 
	 * Возвращаемое значение функции:
	 *  обьект в котором должны быть 3 свойства -
	 * 	- title - это значение инпута title, каждый 3-й символ которого приведен в верхний регистр
	 * 	- daysBefore - количество дней, сколько осталось до даты события
	 * 	- description - значение инпута desc, в котором удалено каждое слово "типа"
	 * 
	 * В решении нужно обязательно испольовать стрелочные функции */


    const daysTillData = (date) => {
    
         let dateInput = new Date(date).getTime();
         let  dateNow = Date.now();
        
        let  remaining = Math.round((dateInput - dateNow) / (24 * 3600  * 1000));
        console.log(`Осталось дней: ${remaining}`);
          return remaining;
        // return Math.round((new Date(date).getTime() - Date.now()) / (24 * 60 * 60 * 1000))
    };

    let makeDescription = (desci) => {
        let newArray = desci.split(" ");
        for(let i = 0; i < newArray.length; i++ ){
            if(newArray[i] ==="типа" || newArray[i] ==="Типа"){
            //    newArray[i] = newArray[i].replace(newArray[i], "");
         delete newArray[i];
           }
        }
        return newArray.join(" ");
    };
     


	return {
		title: 'example', // makeTitle(title.value)
		daysBefore: daysTillData(date.value), // makeDaysBefore(date.value)
		description: makeDescription(desc.value) // makeDescription(desc.value)
	}
}

btn.disabled = !title.value || !date.value || !desc.value
form.onchange = e => {
	btn.disabled = !title.value || !date.value || !desc.value
}

form.onsubmit = e => {
	e.preventDefault()
	const list = document.querySelector('.events-list')

	const event = makeEventObject(title, date, desc)

	list.insertAdjacentHTML(
		'beforeend',
		`
  <div class="events-list-item">
    <p class="event-list-title">${event.title}</p>
    <p class="event-list-days">${event.daysBefore}</p>
    <p class="event-list-desc">${event.description}</p>
  </div>
  `,
	)
}
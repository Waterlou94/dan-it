// rest in object objects
// rest in arrays

// difference in assigning primitive and reference Types

// clone objects 
// clone arrays

// merge objects
// merge arrays


let a = {
    num: 10
}

// let b = a;
let b = { ...a };

b.num = 30;
a.num = 50;
a.test = 'test';

console.log('a -> ');
console.log(a); // { num: 10 }
console.log('b -> ');
console.log(b); // { num: 10 }
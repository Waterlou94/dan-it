// rest in object objects

let a = [10]
let c = [40, 50, 80];

let b = [...a]; // [10, 40, 50, 80]

b.push(30);
a.push(50);

console.log('a -> ');
console.log(a); // [10, 30, 50]
console.log('b -> ');
console.log(b); // [10, 30, 50]
// nested objects
const user = {
    name: {
        first: 'John',
        last: 'Torres',
        family: {
            dad: 'Jack'
        }
    },
    age: 37
}

// console.log(user.name);
const { name: {
    first: firstName, family: { dad, mom = 'Jenny' } }
} = user;

console.log(user);
console.log(dad);
console.log(mom);
console.log(firstName);

const user = {
  firstName: "John",
  lastName: "Mikel",
  age: 21,
  isAdmin: true,
};

const person = {
  firstName: "Rachel",
  age: 23,
  isAdmin: true,
  //   test: null,
};

const { age, firstName, isAdmin, test } = user;

console.log("User: ", age);
console.log("User: ", firstName);
console.log("User: ", isAdmin);

let {
  age: personAge,
  firstName: personFirstName,
  isAdmin: personIsAdmin,
  test: personTest = 123,
} = person;

console.log("Person: ", personAge);
console.log("Person: ", personFirstName);
console.log("Person: ", personIsAdmin);
console.log("Person: ", personTest);

const figure = {
  x_0_y_0: 12,
  x_0_y_1: 4,
  x_1_y_0: 28,
  x_1_y_1: 10,
};

const {
  x_0_y_0: topLeft,
  x_0_y_1: topRight,
  x_1_y_0: bottomLeft,
  x_1_y_1: bottomRight,
} = figure;

const product = {
  name: "iPhone",
  model: "12ProMax",
  //   shipping: {
  //     free: false,
  //     price: 1200,
  //     currency: "$",
  //   },
};

const product2 = {
  name: "iPhone",
  model: "12mini",
  shipping: {
    free: false,
    price: 1200,
    currency: "$",
  },
};

const {
  shipping: { free = false, price = 0, currency = "USD" } = {},
  shipping,
} = product;

// const shipping = !!product ? (!!product.shipping ? product.shipping : {}) : {};
// const free = !!product
//   ? !!product.shipping
//     ? !!product.shipping.price
//     : 0
//   : 0;
// .shipping.free;
// const price = product.shipping.price;
// const currency = product.shipping.currency;

const productList = [product, product2];

console.log("product list:", productList);
productList.forEach((product) => {
  const {
    shipping,
    shipping: {
      free = false,
      price = 0,
      currency = "USD",
      // nestObj: {
      //   nsetObjOj: {
      //     nestObjObjProp
      //   }
      // }
    } = {},
  } = product;

  // shipping?.nestObj?.prop =

  console.log("Product[ship]: ", shipping);
  console.log("Product[ship]: ", free);
  console.log("Product[ship]: ", price);
  console.log("Product[ship]: ", currency);
});

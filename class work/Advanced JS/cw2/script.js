// Person (firstName, lastName)
// - can make coffee
// - can come to workspace
// - can use wifi


const person = {
    firstName: 'John',
    lastName: 'Ivanov',
    age: 18,

    makeCoffee: function(){
        console.log('Make coffee')
    }
}

console.log(person)
console.log(person.__proto__)
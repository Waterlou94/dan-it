/**
 * Створити лічильники кількості населення для 3х
 * країн: України, Німеччини та Польщі
 * Для всіх країн населення спочатку дорівнює 0
 *
 * Збільшити населення на 1_000_000 для
 * України - двічі
 * Німеччини - чотири рази
 * Польщі - один раз
 *
 * При кожному збільшенні виводити в консоль
 * 'Населення COUNTRY_NAME збільшилося на X і становить Y'
 *
 * ADVANCED:
 * - Кожну секунду збільшувати населення України на 100_000 та також
 * виводити в консоль 'Населення COUNTRY_NAME збільшилося на X і становить Y'
 *
 */

function makeCountryCounter(countryName) {
  let count = 0;
  function setPopulation(value) {
    count += value;
    console.log(
      `Населення ${countryName}} збільшилося на ${value} і становить ${count}`
    );
  }
  return setPopulation;
}


let populationUkraine = makeCountryCounter('Ukraine');
populationUkraine(10000)
populationUkraine(500000)
let populationGermany = makeCountryCounter('Germany');
populationGermany(4500000)
populationGermany(4400000)
populationGermany(4600000)
populationGermany(4700000)
let populationPoland = makeCountryCounter('Poland');
populationPoland(200000)

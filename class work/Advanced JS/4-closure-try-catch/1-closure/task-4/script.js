/**
 * Відрефакторити код позбавившись від дублювання коду
 * 
 * Для цього створити функцію createZoom, яка буде приймати назву селектора
 * (selector) та значення зуму (zoomValue)
 * 
 * ADVANCED:
 * Після створення зуму дати можливість викликати цю функцію, щоб дізнатися її
 * параметри та елемент
 * 
 * Наприклад, 
 * const zoomOut = createZoom('#zoom-out', 0.8);
 * console.log(zoomOut()) // { zoom: 0.8,  }
 */

let counter = 0;

const zoomOut = document.querySelector('#zoom-out');

zoomOut.addEventListener('click', function () {
    const fontSize = getComputedStyle(document.body).fontSize;
    const fontSizeValue = parseInt(fontSize);
    console.log(fontSizeValue);
    document.body.style.fontSize = `${fontSizeValue * 0.8}px`;
});


const zoomIn1 = document.querySelector('#zoom-in-1');

zoomIn1.addEventListener('click', function () {
    const fontSize = getComputedStyle(document.body).fontSize;
    const fontSizeValue = parseInt(fontSize);
    console.log(fontSizeValue);
    document.body.style.fontSize = `${fontSizeValue * 1.5}px`;
});

const zoomIn2 = document.querySelector('#zoom-in-2');

zoomIn2.addEventListener('click', function () {
    const fontSize = getComputedStyle(document.body).fontSize;
    const fontSizeValue = parseInt(fontSize);
    console.log(fontSizeValue);
    document.body.style.fontSize = `${fontSizeValue * 2}px`;
});
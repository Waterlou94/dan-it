/**
 * Рахувати кількість натискань на пробіл, ентер,
 * шифт та альт клавіші
 * Відображати результат на сторінці
 *
 * Створити функцію, яка приймає тільки
 * назву клавіші, натискання якої потрібно рахувати,
 * а сам лічильник знаходиться в замиканні цієї функції
 * (https://learn.javascript.ru/closure)
 * id елемента, куди відображати результат має назву
 * "KEY-counter"
 *
 */


function createCounter(keyName) {
let count = 0;
    return () => ++count;
}


const control = createCounter('Пробел');
const enter = createCounter('Энтер');
const shift = createCounter('Шифт');

const enterSpan = document.getElementById('enter-counter');
const spaceSpan = document.getElementById('control-counter');
const shiftSpan = document.getElementById('shift-counter');



document.addEventListener('keydown', (e) => {
    console.log(e);
    switch(e.key) {
case 
'Enter':
enterSpan.innerText = enter();
break;
case 
'Shift':
shiftSpan.innerText = shift();
break;

case 
'Control':
spaceSpan.innerText = control();
break;
    }
})


/**
 * Доповнити функцію authenticate, яка буде резолвити успішно
 * проміс, якщо передали username: 'admin', password: '123', та
 * повертати помилку 'Invalid data' в іншому випадку
 *
 */


// решение

const userDb = [{
  userName: 'Gogi',
  password: '12345'},
  { userName: 'admin',
    password: '123',
  }]

const authenticate = (username, password) => {
  return new Promise((resolve, reject) => {
 setTimeout(() => {

const findedUser = userDb.find((currentUser) =>{
return currentUser.userName === username;
})
if(!findedUser){
  reject (new Error('User not found'));
}
if( findedUser.password === password) {
resolve (findedUser);
} else {
  reject (new Error('Password incorrect'));
}
 }, 2000);
  });
};


authenticate('admin', '123')
.then((data) =>{
  console.log(data);
})
.catch((err) => {
  console.log('Invalid data');
})
// # Завдання
// Написати функціонал логіну на сайт
// * при натисканні на кнопку `Login` перевіряються введені дані
// * через 1 секунду, якщо дані валідні, то виводиться модальне вікно з текстом Success
// * інакше в блок помилок на сторінці виводиться повідомлення _Invalid data_

const USERS = [
  ["admin", "123"],
  ["john", "qwe"],
  ["jack", "asd"],
  ["marry", "123"],
];

const authenticate = (username, password) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (username === "admin" && password === "123") {
        resolve(new Error("Invalid data"));
      } else {
        reject();
      }
    }, 2000);
  });
};

function loginHandler(event) {
  event.preventDefault();
  let username = document.getElementById("username").value;
  let password = document.getElementById("password").value;
  
  authenticate(username, password)
    .then(() => null)
    .then(() => alert("Success"))
    .catch((err) => {
      let errorContainer = document.querySelector(".errors");
      errorContainer.textContent += err.message;
    });
}

const elButton = document.querySelector("btn");
elButton.addEventListener("click", loginHandler);

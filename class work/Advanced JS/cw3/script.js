class Devices {
    constructor(creator, model, price) {
        this.creator = creator;
        this.model = model;
        this._price = price;
    }
    getName() {
        this.creator + this.model
    }

    get price() {
        return this._price * 1.2;
    }
    set price(value) {
        console.log('Price read only')
        this._price = value;
    }

}
class Mobile extends Devices {
    constructor(creator, model, price, nfc) {
        super(creator, model, price);
        this.nfc = nfc;
    }
    getShipping() {
        return this.price > 10000 ? this.price * 0.01 : this.price * 0.03;
    }
}
class Tablet extends Devices {
    constructor(creator, model, price, sim) {
        super(creator, model, price);
        this.sim = sim;
    }
    getShipping() {
        return this.price > 20000 ? 0 : this.price * 0.01;
    }
}
class Laptop extends Devices {
    getShipping() {
        return this.price > 30000 ? 0 : 200;
    }
}
class PremMobile extends Mobile{
    get price() {
        return this._price * 1.35;
    }
}

const telephone = new Mobile("Samsung", "S9", 20000, true);
const tablet = new Tablet("Lenovo", "New", 20000, true);
const laptop = new Laptop("MSI", "Smart", 40000);
const huawei = new PremMobile("Huawei", "PSmart", 20000, true);
console.log(telephone);
console.log(telephone.price);
console.log(telephone.getShipping());
console.log(huawei);
console.log(huawei.price);
console.log(huawei.getShipping());



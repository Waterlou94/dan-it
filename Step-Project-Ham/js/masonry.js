// Masonry layout IMG
$(document).ready(function() {
    $('.grid').masonry({
    columnWidth: 25,
    gutter: 10,
    itemSelector: '.grid-item'
    }).imagesLoaded(function() {
    $('.grid').masonry('reload');
    });
    });



// scroll Up btn
    $(document).ready(function(){   
        $(window).scroll(function () {
            if ($(this).scrollTop() > 600) {
                $('.scrollUp').fadeIn(500);
            } else {
                $('.scrollUp').fadeOut(500);
            }
        });
        $('.scrollUp').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 1000);
            return false;
        });
    });


    $(document).ready(function() {
        $("a.list_link").click(function(){
            $("html, body").animate({
                scrollTop: $($(this).attr("href")).offset().top + "px"
            }, {
                duration: 1500,
                easing: "swing"
            });
            return false;
        });
    });



// hide btn news
$(function () {
$('#slide').click(function () {
    $('.breaking_news').slideToggle(1000);

});
});


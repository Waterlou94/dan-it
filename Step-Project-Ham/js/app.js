'use strict';

console.clear();


//first section tabs
const liBtn = document.querySelectorAll(".tabs-title");
const tabArticle = document.querySelectorAll(".tabs-article");

liBtn.forEach(item => {
  item.addEventListener("click", function () {
    let currentBtn = item;
    let tabId = currentBtn.getAttribute("data-tab");
    let currentTab = document.querySelector(tabId);

    if (!currentBtn.classList.contains("active")) {
      liBtn.forEach((item) => {
        item.classList.remove("active");

      });

      tabArticle.forEach((item) => {
        item.classList.remove("active");
      });

      currentBtn.classList.add("active");
      currentTab.classList.add("active");
    }
  });
});

document.querySelector(".tabs-title").click();


// // AMAZING WORK TABS
// const blockItems = document.querySelectorAll(".blocks-item");
// const listItems = document.querySelectorAll(".our_amazing_list_item");

// // функция фильтрации элементов по классу и дата-аттрибуту
// function filterTab () {
//   listItems.forEach((item => {
    
//   item.addEventListener ('click', event =>{
// const targetTab = event.target.dataset.tab;
// const target = event.target; // кнопка на которой сработает клик



// if(target.classList.contains('our_amazing_list_item')) {
//   listItems.forEach((item) => {
//     item.classList.remove('active-tab');
//     target.classList.add('active-tab');
//   }); 
  
 
// }
// listItems.forEach((item) => {
//   item.classList.remove('active-tab');
//   target.classList.add('active-tab');
// });

// if(targetTab === 'all'){
//   getItems('blocks-item');  
// }

// if(targetTab === 'graphic'){
//   getItems(targetTab);
// }
// if(targetTab === 'design'){
// getItems(targetTab);
// }
// if(targetTab === 'landing'){
//   getItems(targetTab);
// }
// if(targetTab === 'wordpress'){
//   getItems(targetTab);
// }

//   });
// }));

// }

// filterTab();


// // функция проверки класса у item
// function getItems (className) {
//   blockItems.forEach((item) => {
   
//     if (item.classList.contains(className) && !item.classList.contains('hidden')) {
//       item.style.display = 'block';
      
//     } else {
//       item.style.display = 'none';
//       // item.classList.add('hidden');
      
//     }
//    });
// }
// document.querySelector(".our_amazing_list_item").click();

// // LOAD MORE BTN
// const btn = document.querySelector('.btn_load');






// let hiddenImg = document.querySelectorAll('.hidden');

// hiddenImg.forEach(item =>{
// item.style.display = 'none';
// });
// function hiddenCloseclick() {
//   getItems(targetTab);
//   hiddenImg.forEach((hiddenItem) => {
//     if (hiddenItem.style.display === "none"){
//       hiddenItem.style.display = "block";
//   } else {
//     hiddenItem.style.display = "none"}
//   });

// // btn.remove(); 
// }

//   btn.addEventListener('click', hiddenCloseclick);
  



 const liItems = document.querySelectorAll('.our_amazing_list_item');
const blockItems = document.querySelectorAll('.blocks-item');
const ulList = document.getElementById('ulList');
ulList.addEventListener('click', (event) => {
    let target = event.target;

    liItems.forEach((elem) => {
      elem.classList.remove('active-tab');
    });
    target.classList.add('active-tab');
    let tabCategory = target.dataset.tab;

    blockItems.forEach((item)=>{
        item.classList.add('hidden');
        let cardCategory = item.dataset.tab;
        let secondAttr = item.getAttribute('data-content');

        if(tabCategory === 'all' && secondAttr === null) {
            item.classList.remove('hidden');
        }else if(tabCategory === cardCategory && secondAttr === null){
            item.classList.remove('hidden');
        }
    });
});

/*Обработчик по клику на кнопку Load more*/
const btn = document.querySelector('.btn_load');
const loader = document.getElementById('preloader');
let x = false;

btn.addEventListener('click', () => {
  loader.style.display = 'block'; // показываем loader
  btn.style.display = 'none';
  
  if(!x){
    setTimeout(loadMore, 2500);  
    x = true;
    }else{
    
    setTimeout(elseLoadMore, 2500); // setTimeout после чего запуск функции добавления 12 картинок
    x = false;
    }  
});
  


function loadMore () { 
  loader.style.display = 'none';
  blockItems.forEach(item => { 
   item.removeAttribute('data-content');
  });


  liItems.forEach((item)=> {
      if (item.classList.contains("active-tab")) {
      let dataTab = item.getAttribute("data-tab");
          blockItems.forEach((item)=>{
              let dataAttr = item.getAttribute("data-tab");
              if(dataTab === dataAttr){
                  item.classList.remove('hidden');
              }else if(dataTab === "all"){
                  item.classList.remove('hidden');
              }
          });
      }
  });
  btn.style.display = 'block';
}



function elseLoadMore () { 
  loader.style.display = 'none';
 blockItems.forEach(item => { 
   item.removeAttribute('data-cont');
  });


  liItems.forEach((item)=> {
      if (item.classList.contains("active-tab")) {
      let dataTab = item.getAttribute("data-hidden");
          blockItems.forEach((item)=>{
              let dataAttr = item.getAttribute("data-hidden");
              if(dataTab === dataAttr){
                  item.classList.remove('hide');
              }else if(dataTab === "all"){
                  item.classList.remove('hide');
              }
          });
      }
  });
  btn.remove();
}

document.querySelector(".our_amazing_list_item").click();






// SLIDER
// при клике на фотку, отображать профиль

const smallSlide = document.querySelectorAll(".person_small_block");
const personCard = document.querySelectorAll(".person_item");
const cursorBtn = document.querySelectorAll('.cursor');

smallSlide.forEach(item => {
  item.addEventListener("click", function () {
    let activePerson = item;
    let person = activePerson.getAttribute("data-slide");
    let activePic = document.querySelector(person);

    if (!activePerson.classList.contains("active2")) {
      smallSlide.forEach((item) => {
        item.classList.remove("active2");
      });

      personCard.forEach((item) => {
        item.classList.remove("active2");
      });

      activePerson.classList.add("active2");
      activePic.classList.add("active2");
    }
  });
});



document.querySelector(".person_small_block").click();


// SLIDER on btn

// let card = document.querySelectorAll('.person_item');
// let counter = 0;
// function slider () {
//   for (let i = 0;  i < card.length; i++) {
//     card[i].classList.remove('active2');
//   }
//   card[counter].classList.add('active2');
// }
// slider ();

// const cursorLeft = document.querySelector('.left-cursor');
// cursorLeft.addEventListener('click', () => {
//   if(counter - 1 === -1) {
//     counter = card.length - 1;
//   }else {
//     counter--;
//   }
//   slider();
// });

// const cursorRight = document.querySelector('.right-cursor');
// cursorRight.addEventListener('click', () => {
//   if(counter + 1 === card.length) {
//     counter = 0;
//   }else {
//     counter++;
//   }
//   slider();
// });


const nav = document.querySelectorAll('.person_small_block');
const slides = document.querySelectorAll('.person_item');
const rightCursor = document.querySelector('.right-cursor');
const leftCursor = document.querySelector('.left-cursor');

let currentSlide = 0;

nav.forEach((item, i ) => {
  item.addEventListener('click', () => {
    currentSlide = i;
    nav[currentSlide].classList.add('active2');
    slides[currentSlide].classList.add('active2');

  });

});

rightCursor.addEventListener('click', () => {
  nextSlide(currentSlide);
});

leftCursor.addEventListener('click', () => {
  previousSlide(currentSlide);
});

function nextSlide() {
    goToSlide(currentSlide+1);
}

function previousSlide() {
    goToSlide(currentSlide-1);
}

// функция перехода к след. слайду
function goToSlide(n){
    hideSlides();
    currentSlide = (n+slides.length)%slides.length; // формула определения текущего слайда
    showSlides();
}

// функция скрывает слайды
function hideSlides(){
    slides[currentSlide].className = 'person_item';
    nav[currentSlide].className = 'person_small_block';
}

// функция определяет к каким классам добавить active2
function showSlides(){
    slides[currentSlide].className = 'person_item active2';
    nav[currentSlide].className = 'person_small_block active2';
}





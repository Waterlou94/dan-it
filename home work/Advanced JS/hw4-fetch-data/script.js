//Обьясните своими словами, что такое AJAX и чем он полезен при разработке на Javascript.
//С помощью этой технологии можно осуществлять взаимодействие с сервером без необходимости перезагрузки страницы. Это позволяет обновлять
//содержимое страницы частично, в зависимости от действий пользователя.

const cards = document.getElementById('cards')
const films = fetch("https://ajax.test-danit.com/api/swapi/films")
    .then((rsp) => {
        if (rsp.ok) { return rsp.json() }
        throw new Error("No data arrived!")
    })
    .then(Arr => {
        Arr.forEach((item, index) => {
            let { episodeId: episode, name: title, openingCrawl: opening, characters } = item;
          cards.insertAdjacentHTML('beforeend', `
         
              <div class="grid">
                <div class="grid-item novice">
                <ion-icon name="rocket"></ion-icon>
                  <h1 class="film-title">TITLE: ${title}</h1>
                  <h3 class="film-episode">episode_id: ${episode}</h3>
                  <h3>opening_crawl:</h3> ${opening}
                  <div class="film-characters" id="${index}"><p class="char">Characters:</p></div>
                </div>

        
              </div>
            
          
            `)
            characters.forEach((item, i) => {
                item = fetch(`https://ajax.test-danit.com/api/swapi/people/${i}`)
                    .then((rsp) => {
                        if (rsp.ok) { return rsp.json() }
                        throw new Error("No data arrived!")
                    })
                    .then(res => {
                        let { name } = res;
                        let filmChar = document.getElementById(`${index}`);
                        let p = document.createElement('p')
                        filmChar.append(p)
                        p.innerText = (name)
                        // p.style.display = 'flex'
                        // p.style.flexWrap = 'wrap'
                        
                    })
                    .catch((err) => {
                        throw new Error(err)
                    })
            })
        })
    })
    .catch((err) => {
        throw new Error(err)
    })
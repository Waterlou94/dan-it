'use strict'
/* Прототипное наследование это возможность создавать  создавать объекты на основе других объектов.
Например,  у нас есть объект  со своими свойствами и методами, и мы хотим создать другой объекты, 
чтоб использовать то, что есть у  первого объекта, не копировать/переопределять его методы, 
а просто создать новый объект на его основе. Так работает ООП.
*/



class Employee {
    constructor(name, age, salary){
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name(){
        return this._name;
    }

    set name(name) {
this._name = name;
    }

    get age(){
        return this._age;
    }

    set age(age){
        this._age = age;
    }

    get salary(){
        return this._salary;
    }

    set salary(value) {
        this._salary = value;
    }

}



class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary)
        this._lang = lang;
        
    }

    get lang() {
       return this._lang; 
    }

    set lang(lang){
        this._lang = lang;
    }


    get salary() {
        return this._salary * 3;
    }

    set salary(value){
        return this._salary = value;
    }
    
}


const programmerOne = new Programmer('Vasya', 26, 2500, 'Javascript, Java, C++')
console.log(programmerOne);
console.log(programmerOne.salary);

const programmerTwo = new Programmer('Gogi', 29, 1500, 'Python')
console.log(programmerTwo);
console.log(programmerTwo.salary);





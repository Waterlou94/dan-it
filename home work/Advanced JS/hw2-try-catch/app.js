"use strict"


// Конструкция try...catch пытается выполнить инструкции в блоке try, и, в случае ошибки, выполняет блок catch. Эти контсрукции необходимы для отлавливаня ошибок и пробрасывания их в случае если в блоке try обнаружена ошибка, мы ее обнаружим в блоке try.


const books = [{
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70
   },
   {
      author: "Скотт Бэккер",
      name: "Воин-пророк",
   },
   {
      name: "Тысячекратная мысль",
      price: 70
   },
   {
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
   },
   {
      author: "Дарья Донцова",
      name: "Детектив на диете",
      price: 40
   },
   {
      author: "Дарья Донцова",
      name: "Дед Снегур и Морозочка",
   }
];


let root = document.getElementById('root');

let ul = document.createElement('ul');
root.appendChild(ul);


books.map((key) => {

   if (key.author && key.name && key.price) {
      let li = document.createElement('li');
      li.innerHTML = key.author + ', ' + key.name + ', ' + key.price;
      ul.appendChild(li);
   }


   if (!key.author) {
      try {
         if (!el.author) {
            throw new Error();
         }
      } catch (e) {
         console.error(` ${key.author} not found `);
      }


      try {
         if (!key.name) {
            throw new Error();
         }
      } catch (e) {
         console.error(`${key.name} not found `);
      }
      try {
         if (!key.price) {
            throw new Error();
         }
      } catch (e) {
         console.error(` ${key.price} not found `);
      }

   }

});
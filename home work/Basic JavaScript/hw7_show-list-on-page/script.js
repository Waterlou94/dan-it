'use strict';
/* Document Object Model, сокращённо DOM – объектная модель документа, которая представляет все содержимое страницы в виде объектов, которые можно менять и
предоставляет объекты для манипуляций со страницей. */

function filterArrList(arr, parent = document.body) {

    const mapArray = `<ul> ${arr.map(list => `<li> ${list}</li>`).join('')} </ul>`;
    // let splitAndJoin = mapArray.split(',').join('');
    parent.innerHTML = mapArray;

}
 
filterArrList(['Kiev', 'Kharkiv', 'Odessa', 'Lviv', 'Donetsk', 'Sevastopol', 'Yalta']);



const btn = document.querySelector(".btn");
const input = document.querySelectorAll(".input");
const iconEye = document.querySelectorAll(".icon-password");


function showPassword() {
  iconEye.forEach((item, index) => {
    item.addEventListener("click", function () {
      if (item.classList.contains("fa-eye")) {
        item.classList.remove("fa-eye");
        item.classList.add("fa-eye-slash");
        input[index].type = "text";
      } else {
        item.classList.remove("fa-eye-slash");
        item.classList.add("fa-eye");
        input[index].type = "password";
      }
    });   
  });

  
}
showPassword();

btn.addEventListener('click', () =>{
function passwordValidate () {
  const password = document.getElementById('password').value;
  const passwordCheck = document.getElementById('passwordCheck').value;
  let message = document.getElementById('message');


    if(password.length !== 0 && passwordCheck.length !== 0) {
      if (password === passwordCheck) {
        // создание модального окна
        const createModal = function() {
          const modal = document.createElement('div');
          modal.className = 'modal-wrapper';
            modal.style.display = 'flex';
        
          modal.insertAdjacentHTML(
            'beforeend',
            `<div class="modal">
                    <button class="modal-close">x</button>
                    <img src="./wlcm2.jpg" alt="welcome" width="512" height="300">
                </div>`,
          );
        
          modal.addEventListener('click', function (event) {
            if ( event.target.classList.contains('modal-close')) {
              modal.remove();
            }
          });
        
          return modal;
        };
        const modalInBody = createModal();
        document.body.prepend(modalInBody);
        
        
        message.textContent = 'Пароли совпадают!';
        message.style.width = '135px';
        message.style.marginBottom = '15px';
        message.style.marginTop = '-10px';
        message.style.backgroundColor = '#02d44c';
        // ФУНКЦИЯ СОЗДАНИЯ МОДАЛЬНОГО ОКНА
      }  else  { 
        message.textContent = 'Пароли не совпадают!';
      message.style.width = '156px';
      message.style.marginBottom = '15px';
      message.style.marginTop = '-10px';
      message.style.backgroundColor = 'red';
      
    }
    
  } else {
    alert('Пароль не может быть пустым!');
  }
  

}
passwordValidate();

});



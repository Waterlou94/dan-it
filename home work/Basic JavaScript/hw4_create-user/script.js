"use strict";

function createNewUser(firstName, lastName, birthday) {
  let newUser = {
    firstName,
    lastName,
    birthday,
    getLogin: function () {
      let newLogin =
        this.firstName[0].toLowerCase() +
        this.lastName.toLowerCase();
      return newLogin;
    },
    getAge: function () {
      let date = new Date ();
      let age = new Date(birthday);
      let userAge = date.getFullYear() - age.getFullYear();
      return userAge;
    },
    getPassword: function () {
      let newPassword = 
      this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear();
  return newPassword;
    }
    
  };
  return newUser;
}

let result = createNewUser(
  prompt("Как ваше имя?"),
  prompt("Как ваша фамилия?"),
  prompt("Введите вашу дату рождения", "dd.mm.yyyy")
);
console.log(result);
alert(`Ваш логин: ${result.getLogin()}`);
alert(`Ваш возраст: ${result.getAge()} лет!`);
alert(`Ваш пароль: ${result.getPassword()}`);

  






/* Функция  полного клонирования объекта (без единой передачи по ссылке). 
Копирует свойства в виде объектов и массивов на любом уровне вложенности. */

// let BMW = {
//    motor: 2000,
//    color: 'red',
//    doors: 4,
//    maxSpeed: 350,
//    addInfo: {
//        USB: true,
//        stereoMusic: true,
//        GPS: ['4G', 'Phone station', 'Secure control']
//    },
// };
// function copyObj () {
    
//     let newObj = JSON.parse(JSON.stringify(BMW));
//     return  newObj;

// }


// let res = copyObj();
// res.addInfo.USB = 'ADDITIONAL'; // копирует и изменяет свойства вложенных обьектов второго уровня без ссылки.
// res.motor = 2002;
// res.addInfo.GPS[0] = 'LTE'; 


// console.log(BMW);
// console.log(res);

"use strict";

/*  События клавиатуры предназначены именно для работы с клавиатурой. 
Их нежелательно использовать для input т.к текст может быть вставлен мышкой, при помощи клика или без нажатия клавиши. 
Мобильные устройства также не срабатывают на события клавиатуры, а сразу вставляют текст в поле. 
Обработать ввод на них при помощи клавиатурных событий нельзя. */

const body = document.body;
const btns = document.querySelectorAll(".btn");
let enter = document.querySelector("[data-enter]");
let keyS = document.querySelector("[data-s]");
let keyE = document.querySelector("[data-e]");
let keyO = document.querySelector("[data-o]");
let keyN = document.querySelector("[data-n]");
let keyL = document.querySelector("[data-l]");
let keyZ = document.querySelector("[data-z]");

// Обработчик событый с переданной функцией.
body.addEventListener("keypress", switchButton);


// Функция переключения кнопок и подсветки фона в синий цвет.
function switchButton(event) {
  btns.forEach((button) => {
    if(event.target !== event.key) {
      button.style.backgroundColor = "black";

    }
      // KEY ENTER
    if (event.key === "Enter") {
    enter.style.backgroundColor = "blue";
    }
    // KEY S
    if (event.key === "S" || event.key === "s") {
    keyS.style.backgroundColor = "blue";
    }
    // KEY E
    if (event.key === "E" || event.key === "e") {
    keyE.style.backgroundColor = "blue";
    }
    // KEY 0
    if (event.key === "O" || event.key === "o") {
    keyO.style.backgroundColor = "blue";
    }
    // KEY N
    if (event.key === "N" || event.key === "n") {
    keyN.style.backgroundColor = "blue";
    }
    // KEY L
    if (event.key === "L" || event.key === "l") {
    keyL.style.backgroundColor = "blue";
    }
    // KEY Z
    if (event.key === "Z" || event.key === "z") {
    keyZ.style.backgroundColor = "blue";
    }
  });
}

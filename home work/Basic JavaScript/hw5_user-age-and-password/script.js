"use strict";

// В JS экранирование нам нужно для поиска специальных символов. Для этого перед символом нам нужно добавить \.
function createNewUser(firstName, lastName, birthday) {
  let newUser = {
    firstName,
    lastName,
    birthday,
    getLogin: function () {
      let newLogin =
        this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
      return newLogin;
    },
    getAge: function () {
      let userAge =  Math.floor((new Date().getTime() - new Date(this.birthday.split(".").reverse().join("."))) / (1000 * 3600 * 24) / 365);
      return userAge;
    },
    getPassword: function () {
      // let age = new Date(this.birthday.split(".").reverse().join("."));
      let newPassword =
        this.firstName[0].toUpperCase() +
        this.lastName.toLowerCase() +
        new Date(this.birthday).getFullYear();
      return newPassword;
    },
  };
  return newUser;
}

let result = createNewUser(
  prompt("Как ваше имя?"),
  prompt("Как ваша фамилия?"),
  prompt("Введите вашу дату рождения", "dd.mm.yyy")
);
console.log(result);
alert(`Ваш логин: ${result.getLogin()}`);
alert(`Ваш возраст: ${result.getAge()} лет!`);
alert(`Ваш пароль: ${result.getPassword()}`);




"use strict";

/* Разница между var и let в области видимости(scope). У переменной let - это блок, у var область видимости глобальная -
она игнорирует блок и может быть вызвана даже вне блока.
 Так же var существует до ее инициализации со значением undefined, let не существует до ее инициализации.
 У let нельзя перезаписать заданное значение используя эту же перменную, var - можно.
 н. var num = 10;
    var num = 15; // новое значение var = 15;

    let - выдаст ошибку.
    Такие особенности у var могут некорректно повлиять на код. В то время как блочная область видимости - это удобно.
    Поэтому используют let и const вместо var.
    */




let userName = prompt('Введите ваше имя!', 'Имя пользователя');
while (!isNaN(userName) || userName === "" || userName === null || userName <= 0 || userName === 'Имя пользователя') {
    userName = prompt('Некорректные данные. Введите ваше имя!');
}

let userAge = +prompt('Введите ваш возраст!', 'Возраст пользователя');
while (isNaN(userAge) || userAge === 0 || userAge === null || userAge === "" ) {
    userAge = +prompt('Некорректные данные. Введите ваш возраст!');
    
}

if (userAge < 18) {
    alert('You are not allowed to visit this website.');
} else if (userAge >= 18 && userAge <= 22) {
    let confirmButton = confirm('Are you sure you want to continue?');
    

    if (confirmButton === true) {
        alert(`Welcome, ${userName}!`);
    } else {
        alert('You are not allowed to visit this website.');
    }

} else {
    alert(`Welcome, ${userName}!`);
}
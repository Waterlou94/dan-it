'use strict';

/* Метод forEach является методом перебора значений массива, в отличие от других методов массив - он ничего не возвращает.
 Похож на цикл for of, с разницей в том что цикл for of имеет директивы break и continue,
с помощью который можно остановить цикл или перейти на следующую итерацию. forEach этой возможности не имеет. 
Принимает внутрь callback функцию, которую выполн. для кажд элемента массив. в callback функции принимает 3 аргументы - итерируемый элемент, индекс элемента и сам массив.
*/




let items = ['hello', 'world', 23, '23', null, undefined, [], {}, 11, NaN, Infinity];

function filterBy (arr, type) {
    return arr.filter(item => typeof item !== type);
    
} 

console.log(filterBy(items, 'number'));
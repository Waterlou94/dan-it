"use strict";


/*Циклы используются ля многократного повторения одного участка кода.
 Или когда есть необходимость сделать однотипное действие много раз не дублируя код, используют циклы.  */

let firstNum = 0;

let lastNum = +prompt('Введите целое число!');
while(isNaN(lastNum) || lastNum % 1 !== 0 || lastNum === "" || lastNum === null ) {
    lastNum = +prompt('Вы не ввели целое число! Повторите попытку.');
}

let counter = 0;
for (let i = firstNum; i <= lastNum; i++) {
  if (i % 5 === 0) {
    console.log(i);
    counter++;
  }
}
 if (counter === 0) {
  console.log('Sorry, no numbers');
}


// ДОП ЗАДАНИЕ - простые числа


// let m = prompt('Введите первое число!');
// while (isNaN(m) || m === '' || m === 0 || m === null || m % 1 !== 0) {
//   m = prompt('Вы ввели некорректное значение. Введите первое число!');
// }
// let n = prompt('Введите второе число!');
// while (isNaN(n) || n === '' || n === 0 || n === null || n % 1 !== 0) {
//   n = prompt('Вы ввели некорректное значение. Введите второе число!');
// }

// if (n < m) {
//   let inter = m;
//     m = n;
//     n = inter;
// } // проверка если пользователь ввел 1-е число больше 2-го

// for (let i = m; i <= n; i++) {
//   let counter = 0;
//   for (let j = 1; j <= i; j++) {
//     if (i % j) {
//       continue;
//     }
//     counter += 1;
//   }
//   if (counter === 2) {
//     console.log(i);
//   }
// }
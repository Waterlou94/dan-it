const body = document.body;
const changeThemeBtn = document.getElementById('toggleThemeBtn');
const h1Logo = document.querySelector('.logo');
const h1LogoSpan = document.querySelector('.logo-color');
const navbar = document.querySelector('.navbar');
const mainPic = document.querySelector('img');
const navBarMenuItems = document.querySelectorAll('.navbar-menu-item');
const mainImg = document.querySelector('.main-img');
const footerList = document.querySelector('.footer-list');
const footerLinks = document.querySelectorAll('.footer-link');
const sideBarList = document.querySelector('.sidebar-list');
const sidebarItems = document.querySelectorAll('.sidebar-item');


if (!localStorage.theme) {
    localStorage.theme = 'light';
    body.className = 'light';
    changeThemeBtn.innerText = body.classList.contains('dark') ? 'Выбрать темную тему'
        : 'Выбрать светлую тему';
}

function themeChangeConditions ()  {
    changeThemeBtn.innerText = body.classList.contains('dark') ? 'Выбрать темную тему'
        : 'Выбрать светлую тему';

    document.body.classList.toggle('dark');

    if (localStorage.getItem('theme').includes('dark')) {
        h1Logo.className = 'logoNew';
        h1LogoSpan.className = 'logo-colorNew';
        navbar.className = 'navbarNew';
        mainPic.classList.add('main-pic');
        navBarMenuItems.forEach((item) => {
            item.style.backgroundColor = '#c5702a';
            item.addEventListener('mouseover', (event) => {
                item.style.backgroundColor = '#331a05';
                item.addEventListener('mouseout', () => {
                    item.style.backgroundColor = '#c5702a';
                });
            });
        });
        sideBarList.className = 'sidebar-listNew';
        mainImg.className = 'main-imgNew';
        footerList.className = 'footer-listNew';


        footerLinks.forEach((item) => {
            item.addEventListener('mouseover', () => {
                item.style.color = 'limegreen';
                item.style.borderBottom = '1px solid limegreen';
                item.addEventListener('mouseout', () => {
                    item.style.color = '#000';
                    item.style.borderBottom = 'none';
                });
            });
        });
        sidebarItems.forEach((item) => {
            item.style.backgroundColor = 'black';
            item.addEventListener('mouseover', () => {
                item.style.backgroundColor = 'limegreen';
                item.addEventListener('mouseout', () => {
                    item.style.backgroundColor = '#000';
                });
            });
        });


    } else {
        document.body.classList.remove('dark');
        h1Logo.className = 'logo';
        h1LogoSpan.className = 'logo-color';
        navbar.className = 'navbar';
        mainPic.classList.remove('main-pic');

        navBarMenuItems.forEach((item) => {
            item.style.backgroundColor = '#35444F';
            item.addEventListener('mouseover', (event) => {
                item.style.backgroundColor = '#222F3A';
                item.addEventListener('mouseout', () => {
                    item.style.backgroundColor = '#35444F';
                });

            });

        });

        sideBarList.className = 'sidebar-list';
        mainImg.className = 'main-img';
        footerList.className = 'footer-list';

        footerLinks.forEach((item) => {
            item.addEventListener('mouseover', () => {
                item.style.color = '#34a6db';
                item.style.borderBottom = '1px solid #34a6db';
                item.addEventListener('mouseout', () => {
                    item.style.color = '#000';
                    item.style.borderBottom = 'none';
                });
            });
        });

        sidebarItems.forEach((item) => {
            item.style.backgroundColor = 'white';
            item.addEventListener('mouseover', () => {
                item.style.backgroundColor = '#DBDBDB';
                item.addEventListener('mouseout', () => {
                    item.style.backgroundColor = 'white';
                });
            });
        });


    }
}

function changeTheme () {
    if (localStorage.getItem("theme") === "dark") {
        localStorage.setItem("theme", "light");
    } else {
        localStorage.setItem("theme", "dark");
    }

    themeChangeConditions();
}


changeThemeBtn.addEventListener('click', changeTheme);


window.onload = themeChangeConditions;
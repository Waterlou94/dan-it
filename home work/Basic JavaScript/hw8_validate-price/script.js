'use strict';

//переменные
const body = document.body;
const input = document.querySelector("input");
const button = document.createElement("button");
const span = document.createElement("span");


//функция вывода цены и создания/удаления кнопки
function getVal() {
  if (input.value > 0) {
    body.before(span);
    body.before(button);
    span.textContent = `Текущая цена: ${parseInt(input.value)} $`; 
    button.textContent = "X";
    // марджины input и button
    span.style.marginLeft = "8px";
    span.style.marginRight = "8px";
    button.style.marginTop = '5px';
    button.style.color = 'red';
    input.style.color = "#03c03c";
  

  } else {
    input.style.borderColor = "red";
    input.style.transition = "0.5s";
    input.style.borderWidth = "2px";
    body.after(span);
    span.textContent = "Please enter correct price";
  }
  button.addEventListener("click", function () {
    span.remove();
    button.remove();
    input.value = "";
    
  });
}

// функция стилей по фокусу
function onFocus() {
  input.addEventListener("mouseover", function () {
    input.style.borderColor = "#03c03c";
    input.style.transition = "0.5s";
    input.style.borderWidth = "2px";
  });
  input.addEventListener("mouseout", function () {
    input.style.borderColor = "";
    input.style.transition = "0.5s";
    input.style.borderWidth = "";
  });
}

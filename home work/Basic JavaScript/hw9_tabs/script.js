const liBtn = document.querySelectorAll(".tabs-title");
const tabArticle = document.querySelectorAll(".tabs-article");

liBtn.forEach(item => {
  item.addEventListener("click", function () {
    let currentBtn = item;
    let tabId = currentBtn.getAttribute("data-tab");
    let currentTab = document.querySelector(tabId);

    if (!currentBtn.classList.contains("active")) {
      liBtn.forEach((item) => {
        item.classList.remove("active");
      });

      tabArticle.forEach((item) => {
        item.classList.remove("active");
      });

      currentBtn.classList.add("active");
      currentTab.classList.add("active");
    }
  });
});

document.querySelector(".tabs-title").click();



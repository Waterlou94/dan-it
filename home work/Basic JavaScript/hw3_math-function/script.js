"use strict";


/* Функции помогают нам использовать написанный код в любом месте документа и 
являются основными строительными блоками программы. Функции также используются для того чтоб не дублировать наш код.

Аргументы функции - это значения, которые принимает функция при вызове и передает их в параметры.
После чего эти аргументы будут участовать в теле функции.
В качестве аргумента функции может быть также другая функция.*/



function calcFn () {
    let firstNum = prompt('Введите первое число!');
    while(isNaN(firstNum) || firstNum === "" || firstNum === null   ) {
        firstNum = prompt('Некорректные данные! Введите первое число!');
    }
    let secondNum = prompt('Введите второе число!');
    while(isNaN(secondNum) || secondNum === "" || secondNum === null  ) {
        secondNum = prompt('Некорректные данные! Введите второе число!');
    }
    let calc = prompt('Выберите математическую операцию', 'Сложение: +, Вычитание: -, Умножение: *, Деление: /');
    while (calc !== '+' &&  calc !== '-' && calc !== '*' && calc !== '/' ) {
        calc = prompt('Вы не выбрали математическую операцию. Повторите снова!', '+, -, *, /');
    }
    
    if (calc === "+") {
        return +firstNum + +secondNum;
    }  else if (calc === "-") {
        return firstNum - secondNum;
    }  else if (calc === "*") {
        return firstNum * secondNum;
    }  else if (calc === "/") {
        return firstNum / secondNum;
    }
    
    
}


let result = calcFn();
document.write(result);
console.log(typeof result, result);
// "use strict";

const stopBtn = document.querySelector(".btn1");
const contBtn = document.querySelector(".btn2");
let index = 0;

function makeSlideShow() {
  const slides = document.getElementById("slides");
  const slidesInner = slides.querySelector(".slides-inner");
  const images = slidesInner.querySelectorAll("img");

  let interval = setInterval(function () {
    index++;
    if (index === images.length) {
      index = 0;

    }
    
    slidesInner.style.transform = `translate3d(${index * -1200}px, 0, 0)`;
  }, 2500);

  stopBtn.addEventListener("click", () => {
    clearInterval(interval);
  });

  contBtn.addEventListener("click", makeSlideShow);
}

makeSlideShow();

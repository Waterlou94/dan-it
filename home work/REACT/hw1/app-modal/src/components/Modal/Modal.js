import React, { PureComponent } from 'react';



class Modal extends PureComponent {
   
    

    render() {
        const { isOpenFirst, isOpenSecond, closeModal, yesBtn, firstModalHeader, secondModalHeader, firstModalText, secondModalText, actionsFirstModal } = this.props; 
        
        
       
        return (
            <div>
                {isOpenFirst && <div className='modal'>
      <div className='background' onClick={() => closeModal()}></div>
      <div className='content'>
        <div className='content-top'>
        <p className='deleteWarnings'>{firstModalHeader}</p>
        <button className='btnCloseModal' onClick={() => closeModal()}>X</button>
      </div>
        <p className='deleteDescr'>{firstModalText}</p>
        <div className='content-bottom'>
       {actionsFirstModal}
        </div>

      </div>
      </div>}



      {/* second modal render */}
      {isOpenSecond && <div className='modal'>
      <div className='background' onClick={() => closeModal(true)}></div>
      <div className="modal-content">
      <div className="modal-header">
        <h2>{secondModalHeader}</h2>
        <span className="close" onClick={() => closeModal(true)}>&times;</span>
      </div>
      <div className="modal-body">
        <p>{secondModalText}</p>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nulla repellendus nisi, sunt consectetur ipsa velit
          repudiandae aperiam modi quisquam nihil nam asperiores doloremque mollitia dolor deleniti quibusdam nemo
          commodi ab.</p>

          <div className="checkbox-block">
            <label className="checkbox-label" >Agree with our <span className="checkbox-span">Terms & Conditions</span>
        <input className="checkbox" id="checkbox-id" type="checkbox" />
        <span className="check-style"></span>
        </label>
        </div>

      <div className='flex'> 

        <button onClick={() =>yesBtn()} className='agree-btn'>Confirm</button>
      </div>

      </div>
      <div className="modal-footer">
        <h3>Modal Footer</h3>
      </div>
    </div>
      </div>}

            </div>
            
        );
    }
}

export default Modal;

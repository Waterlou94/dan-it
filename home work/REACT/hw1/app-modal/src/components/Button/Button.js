import React, { PureComponent} from "react";
import styles from './Button.module.scss';




class Button extends PureComponent {
    render() {

        const { openModal, firstBtnText, secondBtnText, firstModalBg, secondModalBg } = this.props;

        

        return (
            <div>
                <button style={{'backgroundColor': firstModalBg}} className={styles.firstBtn} onClick={() =>openModal()}>{firstBtnText}</button>
                <button style={{'backgroundColor': secondModalBg}} className={styles.secondBtn} onClick={()=>openModal(true)}>{secondBtnText}</button>
            </div>
        );
    }
}

export default Button;
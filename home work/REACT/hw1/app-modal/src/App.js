import React, { PureComponent } from 'react';
import './App.scss';
import Button from './components/Button/Button';
import Modal from './components/Modal/Modal'



class App extends PureComponent {

state = {
  isOpenFirst: false,
  isOpenSecond: false,
}


openModal = (isSecond = false) => {
  if(isSecond) {
    this.setState({ isOpenSecond: true })
  } else {
    this.setState({ isOpenFirst: true })
  }
}

closeModal = (isSecond = false) => { 
  if(isSecond) {
  this.setState({ isOpenSecond: false })
} else {
  this.setState({isOpenFirst: false})
}
}


yesBtn = (isSecond = false) => {
  if(isSecond) {
    alert('File deleted!')
    this.setState({isOpenFirst: false})
  }else {
    alert('Agreement confirmed!')
    this.setState({isOpenSecond: false})
  }
}
  render() {
    
    const actions = <>
    <button className='confirmBtn btn' onClick={this.yesBtn}>ok</button>
    <button className='cancelBtn btn' onClick={() => this.closeModal()}>cancel</button>
    </>; 

    return (
      <>
     <Button firstModalBg={'tomato'} secondModalBg={'#428bca'} openModal={this.openModal} firstBtnText={'Open first modal'} secondBtnText={'Open second modal'}  />
      <Modal closeModal={this.closeModal} yesBtn={this.yesBtn} isOpenFirst={this.state.isOpenFirst} isOpenSecond={this.state.isOpenSecond} firstModalHeader={'Do you want to delete this file?'} secondModalHeader={'Modal Header'} firstModalText={'Once you delete this file, it wont be posibleto undo this action.<br/> Are you sure you want to delete it?'} secondModalText={'This is my modal'} 
      actionsFirstModal={actions}  />

      </>
    );
  }
}

export default App;
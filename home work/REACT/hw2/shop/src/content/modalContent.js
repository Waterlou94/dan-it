export const modalContent = {
    header: 'Are you ready to add this card to your shopping cart?',
    text: `Click 'Ok' to continue, or 'Cancel' to return`,
    textBtn: 'ADD TO CART',
    colorBtn: '#B3382C', 
}